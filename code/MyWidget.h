#ifndef CPP_MYWIDGET_H
#define CPP_MYWIDGET_H

#include <vector>
#include "QtWidgets/QWidget"
#include "QtWidgets/QApplication"
#include "QtWidgets/QLabel"
#include "QtWidgets/QPushButton"
#include "QtWidgets/QVBoxLayout"
#include "QtWidgets/QHBoxLayout"
#include "QtWidgets/QStackedWidget"
#include "QtWidgets/QFileDialog"
#include "QtMultimedia/QMediaPlayer"
#include "QtMultimediaWidgets/QVideoWidget"


class MyWidget : public QWidget {
    Q_OBJECT
public:
    MyWidget(QWidget *parent = nullptr);

private slots:
    void PlayVideo();

protected:
    bool eventFilter(QObject *watched, QEvent *event) override;

private:
    QMediaPlayer *m_player = nullptr;
    QPushButton *m_playButton = nullptr;
    QWidget *m_menuWidget = nullptr;
    QVideoWidget *m_videoWidget = nullptr;
    QStackedWidget *m_stackWidget = nullptr;
    QLabel *m_menuTitle = nullptr;
    QVBoxLayout *m_menuButtonLayout = nullptr;
    QString m_fileName;
};
#endif // CPP_MYWIDGET_H